﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotCode : MonoBehaviour {

	public float speed;
	public float delay;

	// Use this for initialization
	void Start () {
		Destroy (gameObject, delay);
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (Vector3.forward * Time.deltaTime * speed);
	}
}
