﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

	//sets the tank's movement speed
	public int playerSpeed = 10;
	public int jumpHeight = 2;
	public int maxJumps;
	public int jumpsLeft;
	public Rigidbody2D rb;
	public float groundDistance;
	public bool flipped = false;
	public bool isGrounded;
	public GameObject paused;
	public GameObject currentGame;
	public LayerMask groundLayer;
	public AudioClip jumpSound;

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		//player moves freely as long as button is pressed
		if (Input.GetKey ("left")) {
			transform.position -= transform.right * playerSpeed * Time.deltaTime;
			if (flipped == false) {
				transform.localScale = new Vector3(transform.localScale.x *-1, transform.localScale.y, transform.localScale.z);
				flipped = true;
			}
		}
		if (Input.GetKey ("right")) {
			transform.position += transform.right * playerSpeed * Time.deltaTime;
			if (flipped == true) {
				transform.localScale = new Vector3(transform.localScale.x *-1, transform.localScale.y, transform.localScale.z);
				flipped = false;
			}
		}
		if (Input.GetKeyDown ("up")) {
			//Draws Raycast in Scene window, but not in game window
			Debug.DrawRay(transform.position, Vector3.down * groundDistance, Color.green);
			//checks if player is grounded
			RaycastHit2D hit = Physics2D.Raycast (transform.position, Vector3.down, groundDistance, groundLayer);
			if (hit.collider != null) {
				isGrounded = true;
			} 
			else {
				isGrounded = false;
			}
			//resets jump on grounded
			if (isGrounded == true) {
				jumpsLeft = maxJumps;
				rb.velocity += jumpHeight * Vector2.up;
			}
			//allows for set jumps in air
			if (isGrounded == false && jumpsLeft > 0) {
				jumpsLeft = jumpsLeft - 1;
				rb.velocity += jumpHeight * Vector2.up;
			} 
		}
		//opens pause menu
		if (Input.GetKeyDown ("escape")) {
			paused.SetActive (true);
			currentGame.SetActive (false);
		}
	}
}
